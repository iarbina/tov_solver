!----------------------------------------------------------------------
!  Kind type module 
!----------------------------------------------------------------------
module kind_type  
  use, intrinsic :: iso_fortran_env, wp => real64
  implicit none
  public :: wp
end module kind_type  

!----------------------------------------------------------------------
! Main driver program 
!----------------------------------------------------------------------
program tov_driver
  use kind_type
#ifdef _OPENMP
  use omp_lib
#endif
  implicit none
  integer  :: i, iounit, istat, fdim, ios, nseq, loc
  real(wp), allocatable :: nb(:), pres(:), eden(:)
  real(wp), allocatable :: nbc(:), mass(:), radius(:)
  real(wp), parameter   :: edcf = 1.602176565e33_wp
  integer,  parameter   :: nseq_default = 50
  character(len=1024)   :: filename, msg, arg_seq
  interface
    subroutine tov_solver (sdim, nb, pres, rho, nbc_out, mass, radius)
      use kind_type
      implicit none
      ! i/o variables
      integer,  intent(in)  :: sdim
      real(wp), intent(in)  :: nb(:), rho(:), pres(:)
      real(wp), allocatable, intent(out) :: nbc_out(:), mass(:), radius(:)
    end subroutine tov_solver
  end interface

  print'("Usage: tov_solver nseq eos_file")'
  print'(" - nseq     :  number of stars in the sequence (integer)")'
  print'(" - eos_file :  file with the EOS (default tov_eos.dat)")'
  print'()'

#ifdef _OPENMP
  print'(":: Number of threads: ",g0)', OMP_GET_MAX_THREADS()
#endif
  
  if (command_argument_count() > 0) then
     
     print'(":: Number of arguments passed: ",g0)', command_argument_count()
     
     call get_command_argument (number=1, value=arg_seq, status=istat)

     if (istat > 0) then

        nseq = nseq_default
        print'(":: Number of stars in sequence not supplied, setting default to ",g0)', nseq

     else
        
        read(unit=arg_seq, fmt=*, iostat=istat) nseq
        print'(":: Setting number of stars in sequence to ",g0)', nseq

     end if
     

     call get_command_argument (number=2, value=filename, status=istat)
     
     print'(":: Setting filename to ",g0)', trim(filename)
     
     if (istat > 0) then

        filename = 'tov_eos.dat'
        print'(":: Error with supplied EOS file. Setting default EOS file to ",g0)', trim(filename)

     end if
     
     
   else

     filename = 'tov_eos.dat'
     print'(":: EOS file not supplied. Setting default EOS file: ",g0)', trim(filename)

     nseq = nseq_default
     print'(":: Setting number of stars in sequence to ",g0)', nseq

  end if


  print'(":: Reading file ",g0,"...")', trim(filename)
  
  fdim = get_file_length(trim(filename), .true.)
  
  print'("   |-> EOS file length: ",g0)', fdim

  allocate(nb(fdim), pres(fdim), eden(fdim))
  
  open(newunit=iounit, file=trim(filename))
  read(iounit,*)  ! read heading of file
  do i=1, fdim
    read(iounit,*) nb(i), pres(i), eden(i)
  end do
  close(unit=iounit)

  eden = edcf * eden
  pres = edcf * pres
 
  print'(":: Calling TOV solver ...")'
  call tov_solver (nseq, nb, pres, eden, nbc, mass, radius)
  print'(":: End TOV solver ...")'

  open(newunit=iounit, file='tov_mass_rad.dat')
  write(iounit,'("#",3x,a,6x,a,5x,a)') 'M (Msun)', 'R (km)', 'nB (fm^-3)'
  do i=1, nseq
     write(iounit,'(3(2x,es11.4))') mass(i), radius(i), nbc(i)
  end do
  close(iounit)

  loc = minloc(abs(mass-maxval(mass)), 1)

  print'("   |-> M_max    = ",f6.4," Msun")', mass(loc)
  print'("   |-> R(M_max) = ",f6.3," km")', radius(loc)
  
  print'("Program finished")'

contains

  function get_file_length (filenam, head) result(res)
    character(len=*) :: filenam
    real(wp) :: cols(3)
    logical :: head
    integer :: res
    open(newunit=iounit, file=trim(filenam))
    if (head) read(iounit,*)
    i = 0
    find_file_size: do
      i = i + 1
      read(iounit, fmt=*, iostat=ios, iomsg=msg) cols
      if (ios < 0) exit
    end do find_file_size
    res = i
    if (head) res = res - 1
    close(unit=iounit)
  end function get_file_length

end program tov_driver

!----------------------------------------------------------------------
! TOV equations solver subroutine
!----------------------------------------------------------------------
subroutine tov_solver (sdim, nb, pres, rho, nbc, mass, radius)
  ! Solves TOV equatios for a given EOS
  ! Input:
  ! > sdim: number of stars in the sequence
  ! > nb  : barion density in fm⁻³
  ! > pres: pressure in erg/cm³
  ! > rho : energy density in erg/cm³
  ! Output:
  ! > nbc : set of central densities in fm⁻³
  ! > mass: set of masses in solar masses
  ! > radius: set of radius in cm
  use kind_type
  implicit none
  ! i/o variables
  integer,  intent(in)  :: sdim
  real(wp), intent(in)  :: nb(:), rho(:), pres(:)
  real(wp), allocatable, intent(out) :: nbc(:), mass(:), radius(:)
  ! RK4 variables
  real(wp) :: nb_min, nb_max
  real(wp) :: r0, p0, m0, r, p, m, dr, rho0
  real(wp) :: k1p, k2p, k3p, k4p
  real(wp) :: k1m, k2m, k3m, k4m
  ! constants
  real(wp), parameter :: G = 6.6743e-8_wp           ! cm^3 / g s
  real(wp), parameter :: c = 2.997992458e10_wp      ! cm / s
  real(wp), parameter :: pi = 3.141592653589793_wp
  real(wp), parameter :: Msun = 1.98847e33_wp       ! g
  real(wp), parameter :: half = 0.5_wp
  real(wp), external  :: interp1d
  integer  :: i


  allocate(nbc(sdim), mass(sdim), radius(sdim))

  r0 = 0.0_wp
  dr = 2.0e2_wp
  m0 = 0.0_wp
  
  nb_min = minval(nb); nb_max = maxval(nb)

  if (sdim == 1) then
     nbc = nb_min
  else
     nbc = [ (nb_min + (i-1)*(nb_max-nb_min)/(sdim-1), i=1, sdim) ]
  end if

  !$OMP PARALLEL DO DEFAULT(shared)           &
  !$OMP PRIVATE(i, r0, p0, rho0, m0, r, p, m, &
  !$OMP k1p, k2p, k3p, k4p, k1m, k2m, k3m, k4m)
  stars_sequence: do i=1, sdim
    
    write(6,'(a1,"   |-> Computing star in sequence number ",g0,"/",g0," ...")',advance='no') char(13), i, sdim
    
    r0 = dr
    p0   = interp1d (nbc(i), nb, pres, size(nb))
    rho0 = interp1d (p0, pres, rho, size(pres))
    m0 = dr * dm_dr(r0, rho0)

    rk4_iteration: do 

      rho0 = interp1d  (p0, pres, rho, size(pres))
      k1p  = dr * dp_dr(r0, rho0, m0, p0)
      k1m  = dr * dm_dr(r0, rho0)
      
      rho0 = interp1d  (p0 + half*k1p, pres, rho, size(pres))
      k2p  = dr * dp_dr(r0 + half*dr, rho0, m0 + half*k1m, p0 + half*k1p)
      k2m  = dr * dm_dr(r0 + half*dr, rho0)
      
      rho0 = interp1d  (p0 + half*k2p, pres, rho, size(pres))
      k3p  = dr * dp_dr(r0 + half*dr, rho0, m0 + half*k2m, p0 + half*k2p)
      k3m  = dr * dm_dr(r0 + half*dr, rho0)

      rho0 = interp1d  (p0 + k3p, pres, rho, size(pres))
      k4p  = dr * dp_dr(r0 + dr, rho0, m0 + k3m, p0 + k3p)
      k4m  = dr * dm_dr(r0 + dr, rho0)

      r = r0 + dr
      p = p0 + (k1p + 2.0_wp*k2p + 2.0_wp*k3p + k4p)/6.0_wp
      m = m0 + (k1m + 2.0_wp*k2m + 2.0_wp*k3m + k4m)/6.0_wp
      
      if (p < 0.0_wp) exit

      r0 = r; p0 = p; m0 = m
    
    end do rk4_iteration
      
    mass(i)   = half * (m0 + m) / Msun
    radius(i) = half * (r0 + r) * 1.0e-5_wp

  end do stars_sequence
  !$OMP END PARALLEL DO

  write(6, fmt=*)

contains

  function dm_dr (r, rhor)
    real(wp) :: r, rhor, dm_dr
    dm_dr =  rhor / (c*c) * 4.0_wp * pi * r*r 
  end function dm_dr 

  function dp_dr (r, rhor, massr, presr)
    real(wp) :: r, rhor, massr, presr, dp_dr
    dp_dr = - G * (rhor + presr)  &
            * (massr * c*c + 4.0_wp * pi * r*r*r * presr) &
            / (r*c*c * (r*c*c - 2.0_wp * G * massr))
  end function dp_dr

  function dnu_dr (rhor, presr, dpres_dr)
    real(wp) :: rhor, presr, dpres_dr, dnu_dr
    dnu_dr = - 2.0_wp * dpres_dr / (rhor * presr)
  end function dnu_dr

end subroutine tov_solver

!----------------------------------------------------------------------
! Interpolation function
!----------------------------------------------------------------------
function interp1d (x, xgrid, ygrid, ndim) result(y)
  use kind_type
  implicit none
  integer,  intent(in)  :: ndim
  real(wp), intent(in)  :: x, xgrid(ndim), ygrid(ndim)
  real(wp) :: y, xlo(1), xhi(1), ylo(1), yhi(1)

  call find_interval (x, xgrid, ygrid, size(xgrid), xlo, xhi, ylo, yhi)

  y = lerp(x, [xlo(1), xhi(1)], [ylo(1), yhi(1)])
contains

  subroutine find_interval (xval, x_in, y_in, n, xl, xh, yl, yh)
    integer,  intent(in)  :: n
    real(wp), intent(in)  :: xval, x_in(0:n-1), y_in(0:n-1)
    real(wp), intent(out) :: xl(1), xh(1), yl(1), yh(1)
    logical :: mask(n)
    mask = (x_in(1:n-1) - xval) * (x_in(0:n-2) - xval) <= 0.0_wp
    xl = pack(x_in(0:n-2), mask)
    xh = pack(x_in(1:n-1), mask)
    yl = pack(y_in(0:n-2), mask)
    yh = pack(y_in(1:n-1), mask)
  end subroutine find_interval

  function lerp (x, xl, yl)
    real(wp) :: lerp, x, xl(2), yl(2)
    lerp = (yl(2)-yl(1))/(xl(2)-xl(1))*(x-xl(1))+yl(1)
  end function lerp

end function interp1d
